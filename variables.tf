variable "cidr" {
  description = "The CIDR block for the VPC."
}

variable "public_subnets" {
  description = "List of public subnets"
}

variable "container_port" {
  description = "Container port to expose"
}

variable "private_subnets" {
  description = "List of private subnets"
}

variable "availability_zones" {
  description = "List of availability zones"
}

variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
}

variable "project" {
  description = "Project name"
}

variable "task_cpu" {
  description = "Number of cpu units used by the ECS task"
}

variable "task_memory" {
  description = "Amount (in MiB) of memory used by the ECS task"
}

variable "image" {
  description = "The image used to start a container"
}

variable "environment" {
  description = "Current environment"
}

variable "secrets" {
  description = "Reference to secrets"
}

variable "domain" {
  description = "route53 domain"
}

variable "containers" {
  description = "containers"
  type        = map(object({ name = string }))
}
variable "application" {
  description = "containers"
}

variable "openvpn_users" {

}