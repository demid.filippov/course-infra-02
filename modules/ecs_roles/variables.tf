variable "environment" {
  description = "Current environment"
}
variable "secrets_arn" {
  description = " The ARN of the policy"
}
