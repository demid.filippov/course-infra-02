variable "name" {
  description = "Security group name."
}

variable "vpc_id" {
  description = "ID of VPC."
}

variable "container_port" {
  description = "Container port to expose"
}
