variable "cluster_name" {
  description = "ECS cluster name."
}

variable "service_name" {
  description = "ECS service name"
}

variable "cpu_average_target" {
  description = "The target value for the CPU metric"
}

variable "memory_average_target" {
  description = "The target value for the memory metric"
}

variable "scale_in_cooldown" {
  description = "The amount of time, in seconds, after a scale in activity completes before another scale in activity can start"
}

variable "scale_out_cooldown" {
  description = "The amount of time, in seconds, after a scale out activity completes before another scale out activity can start"
}

variable "max_capacity" {
  description = "The max capacity of the scalable target"
}

variable "min_capacity" {
  description = "The min capacity of the scalable target"
}
