variable "name" {
  description = "Name of the secret"
}
variable "environment" {
  description = "Current environment"
}

variable "secrets" {
  description = "A map of secrets that is passed into the application. Formatted like ENV_VAR = VALUE"
  type        = map(any)
}

variable "ssm_key_prefix" {

}