variable "name" {
  description = "App name"
}

variable "environment" {
  description = "Current environment"
}

variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
}

variable "target_group_arn" {
  description = "Target Group ARN"
}

variable "alb_arn" {
  description = "ALB resource name"
}

variable "cluster_name" {
  description = "ECS cluster name"
}

variable "service_name" {
  description = "ECS service name"
}
