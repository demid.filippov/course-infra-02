variable "environment" {
  description = "Current environment"
}
variable "name" {
  description = "A name for task definition"
}
variable "network_mode" {
  description = "Docker networking mode to use for the containers in the task"
}
variable "requires_compatibilities" {
  description = "Set of launch types required by the task"
}
variable "cpu" {
  description = "Number of cpu units used by the task"
}
variable "memory" {
  description = "Amount (in MiB) of memory used by the task"
}
variable "execution_role_arn" {
  description = "ARN of the task execution role"
}
variable "task_role_arn" {
  description = "ARN of IAM role that allows your Amazon ECS container task to make calls to other AWS services"
}
variable "container_definitions" {
  description = "A list of valid container definitions"
}
