variable "domain" {
  description = "route53 domain"
}

variable "environment" {
  description = "Current environment"
}

variable "alb_dns_name" {
  description = "ALB DNS name"
}
variable "alb_zone_id" {
  description = "ALB zone ID"
}