
variable "environment" {
  description = "Current environment"
}

variable "container_port" {
  description = "Container port to expose"
}

variable "task_cpu" {
  description = "Number of cpu units used by the ECS task"
}

variable "task_memory" {
  description = "Amount (in MiB) of memory used by the ECS task"
}

variable "image" {
  description = "The image used to start a container"
}

variable "vpc_id" {
  description = "Identifier of the VPC in which to create the target group"
}

variable "public_subnets" {
  description = "A list of subnet IDs to attach to the LB"
}

variable "ecs_sg" {
  description = "Security group name for ECS"
}

variable "alb_sg" {
  description = "Security group name for ALB"
}

variable "private_subnets" {
  description = "Current environment"
}

variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
}

variable "secrets" {
  description = "Subnets associated with the service"
}

variable "variables" {
  description = "Variables to print in variable module"
}

variable "domain" {
  description = "route53 domain"
}

variable "project" {
  description = "Project name"
}
variable "application" {
  description = "Application name"
}
variable "containers" {
  description = "containers"
  type        = map(object({ name = string }))
}