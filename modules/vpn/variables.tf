variable "vpn_domain" {}

variable "domain" {}

variable "security_groups" {}

variable "subnet_id" {}

variable "users" {}

variable "openvpn_install_script_location" {
  default = "https://raw.githubusercontent.com/dumrauf/openvpn-install/master/openvpn-install.sh"
}

variable "instance_type" {}

variable "ovpn_config_directory" {}

variable "vpn_pem_file" {}
