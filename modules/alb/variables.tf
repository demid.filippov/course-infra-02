variable "name" {
  description = "Load balancer name."
}

variable "environment" {
  description = "Current environment"
}

variable "security_groups" {
  description = "A list of security group IDs to assign to the LB"
}

variable "subnets" {
  description = "A list of subnet IDs to attach to the LB"
}

variable "vpc_id" {
  description = "Identifier of the VPC in which to create the target group"
}

variable "health_check_path" {
  description = "Destination for the health check request"
}

variable "certificate_arn" {
  description = "certificate_arn"
}