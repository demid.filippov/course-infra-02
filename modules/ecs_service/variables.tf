variable "name" {
  description = "ECS service name"
}

variable "environment" {
  description = "Current environment"
}

variable "cluster_id" {
  description = "ARN of an ECS cluster"
}

variable "task_definition_arn" {
  description = "ARN of an task definition"
}

variable "desired_count" {
  description = "Number of instances of the task definition to place and keep running"
}

variable "min_percent" {
  description = "Lower limit (as a percentage of the service's desiredCount) of the number of running tasks that must remain running and healthy in a service during a deployment"
}
variable "max_percent" {
  description = "Upper limit (as a percentage of the service's desiredCount) of the number of running tasks that can be running in a service during a deployment"
}
variable "launch_type" {
  description = "Launch type on which to run service"
}
variable "scheduling_strategy" {
  description = "Scheduling strategy to use for the service"
}
variable "security_groups" {
  description = "Security groups associated with the task or service"
}
variable "subnets" {
  description = "Subnets associated with the task or service"
}
variable "aws_alb_target_group_arn" {
  description = "ARN of the Load Balancer target group to associate with the service"
}
variable "container_port" {
  description = "Port on the container to associate with the load balancer"
}
variable "container_name" {
  description = "Name of the container to associate with the load balancer"
}
variable "enable_execute_command" {
  description = "Specifies whether to enable Amazon ECS Exec for the tasks within the service"
}
