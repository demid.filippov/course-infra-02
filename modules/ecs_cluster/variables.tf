variable "name" {
  description = "ECS cluster name"
}
variable "environment" {
  description = "Current environment"
}
