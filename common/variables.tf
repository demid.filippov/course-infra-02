variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
}

variable "domain" {
  description = "route53 domain"
}

variable "roles" {
  description = "IAM roles"
}

variable "users" {
  description = "IAM users"
}

variable "containers" {
  description = "containers"
  type        = map(object({ name = string }))
}

variable "project" {
  description = "project"
}

variable "runner_token" {
  description = "runner_token"
}

variable "application" {
  description = "application"
}
variable "important_environments" {
  description = "important_environments"
}
