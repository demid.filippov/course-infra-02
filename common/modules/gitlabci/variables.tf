variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
}

variable "environment" {
  description = "Current environment"
}
variable "vpc_id" {
  description = "Current"
}
variable "subnet_ids_gitlab_runner" {
  description = "Current environment"
}
variable "subnet_id_runners" {
  description = "Current environment"
}
variable "ci_prefix" {
  description = "Current environment"
}
variable "gitlab_runner_version" {
  description = "Current environment"
}
variable "docker_machine_instance_type" {
  description = "Current environment"
}
variable "instance_type" {
  description = "Current environment"
}
variable "gitlab_runner_registration_token" {
  description = "Current environment"
}
variable "locked_to_project" {
  description = "Current"
}
variable "run_untagged" {
  description = "Current"
}
variable "maximum_timeout" {
  description = "Current"
}
