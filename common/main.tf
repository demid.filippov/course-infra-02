module "iam" {
  source = "./modules/iam"
  roles  = var.roles
  users  = var.users
}

module "dns" {
  source = "./modules/dns"
  domain = var.domain
}

module "ecr" {
  source = "./modules/ecr"

  for_each               = var.containers
  name                   = "${var.project}-${var.application}-${each.value.name}"
  important_environments = var.important_environments
  amount_to_keep         = 30
}

resource "aws_default_vpc" "default" {}

resource "aws_default_subnet" "default_az" {
  availability_zone = "${var.region}a"
}

module "gitlabci" {
  source = "./modules/gitlabci"

  vpc_id                           = aws_default_vpc.default.id
  subnet_ids_gitlab_runner         = [aws_default_subnet.default_az.id]
  subnet_id_runners                = aws_default_subnet.default_az.id
  region                           = var.region
  ci_prefix                        = var.project
  environment                      = "ci"
  gitlab_runner_version            = "13.12.0"
  docker_machine_instance_type     = "t3.micro"
  instance_type                    = "t3.micro"
  gitlab_runner_registration_token = var.runner_token
  locked_to_project                = true
  run_untagged                     = false
  maximum_timeout                  = "7200"
}

module "cloudtrail" {
  source = "./modules/cloudtrail"
  prefix = var.project
}

moved {
  from = aws_acm_certificate.wildcard
  to   = module.dns.aws_acm_certificate.wildcard
}

moved {
  from = aws_route53_zone.zone
  to   = module.dns.aws_route53_zone.zone
}

moved {
  from = aws_route53_record.validation
  to   = module.dns.aws_route53_record.validation
}

moved {
  from = aws_acm_certificate_validation.validation
  to   = module.dns.aws_acm_certificate_validation.validation
}
